import java.util.*;

import java.util.ArrayList;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class CurrencyCalculator {

    public double averageValue,standardDeviationValue,coefficientOfVariationValue;
    public float medianValue;
    public ArrayList<Float> dominantValue;
    public int howManyDaysItGrewValue, calculateDroppingDaysValue,howManyDaysItWasConstantValue;


    public void calculateAll(ArrayList values)
    {
        averageValue = average(values);
        standardDeviationValue = standardDeviation(values);
        coefficientOfVariationValue = coefficientOfVariation(values);
        medianValue = median(values);
        dominantValue = dominant(values);
        howManyDaysItGrewValue = howManyDaysItGrew(values);
        calculateDroppingDaysValue = calculateDroppingDays(values);
        howManyDaysItWasConstantValue = howManyDaysItWasConstant(values);
    }

    public String toString(){
        return "Standard deviation: "+standardDeviationValue+"\nCoefficient of variation: "+coefficientOfVariationValue+"\nExchange rate rising days: "+howManyDaysItGrewValue+"\nExchange rate dropping days: "+calculateDroppingDaysValue+"\nExchange rate constant days: "+howManyDaysItWasConstantValue+ "\nMedian: "+medianValue;
    }


    public double average (ArrayList values){

        double sum = 0;
        for (int i=0; i< values.size(); i++) {
            sum += Double.valueOf(values.get(i).toString());
        }
        double avg = sum / values.size();
        return avg;
    }

    public double standardDeviation (ArrayList values){

        double avg = average(values);
        double sum = 0;
        for (int i=0; i< values.size(); i++) {
            sum += pow(Double.valueOf(values.get(i).toString()) - avg, 2);
        }
        double variance = sum / values.size();
        double deviation = sqrt(variance);
        return deviation;
    }

    public double coefficientOfVariation (ArrayList values){

        double deviation = standardDeviation(values);
        double avg = average(values);
        double coefficient = deviation / avg;
        return coefficient;
    }

    float median(ArrayList valuesList) {
        int quantity = valuesList.size();
        ArrayList tempArr = new ArrayList<>(valuesList);

        Collections.sort(tempArr);

        if (quantity % 2 != 0) {
            return (float) tempArr.get(((quantity / 2)));
        } else {
            return ((float) tempArr.get((quantity / 2) - 1) + (float) tempArr.get((quantity / 2))) / 2;
        }
    }

    ArrayList<Float> dominant(ArrayList valuesList) {

        ArrayList<Float> dominantValue = new ArrayList<>();
        Map<Float, Integer> duplicates = new HashMap<>();

        for (Object value : valuesList) {
            if (duplicates.containsKey(value)) {
                duplicates.put((Float) value, duplicates.get(value) + 1);
            } else {
                duplicates.put((Float) value, 1);
            }
        }

        int maxValueInMap = (Collections.max(duplicates.values()));

        if (maxValueInMap == 1) {
            return null;
        } else {
            for (Map.Entry<Float, Integer> entry : duplicates.entrySet()) {
                if (entry.getValue() == maxValueInMap) {
                    dominantValue.add(entry.getKey());
                }
            }

            return dominantValue;
        }
    }

    int howManyDaysItGrew(ArrayList values) {
        int days = 0;
        float firstValue = (float) values.get(0);
        for (int i = 1; i < values.size(); i++) {
            if (firstValue < (float) values.get(i)) {
                days++;
            }
            firstValue = (float) values.get(i);
        }

        return days;
    }

    public int calculateDroppingDays(ArrayList valuesList) {
        int counter = 0;
        float prevVal = 0.0f;
        for (Object value: valuesList) {
            if(prevVal > (float)value)
                counter++;

            prevVal = (float)value;
        }

        return counter;
    }

    int howManyDaysItWasConstant(ArrayList values) {
        int days = 0;
        int inSequence = 0;
        float firstValue = (float) values.get(0);
        for (int i = 1; i < values.size(); i++) {
            if (firstValue == (float) values.get(i)) {
                if (inSequence == 0) {
                    inSequence++;
                    days += 2;
                } else {
                    days += 1;
                }
            } else {
                inSequence = 0;
            }
            firstValue = (float) values.get(i);
        }

        return days;
    }
}