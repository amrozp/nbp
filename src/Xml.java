
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class Xml {

    private float[] mids;
    private String[] effectiveDates;
    private LinkedHashMap<String, Float> values;

    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private LocalDate localDate;

    private LinkedHashMap<String, Float> lastWeek = new LinkedHashMap<>();
    private LinkedHashMap<String, Float> lastTwoWeeks = new LinkedHashMap<>();
    private LinkedHashMap<String, Float> lastMonth = new LinkedHashMap<>();
    private LinkedHashMap<String, Float> lastQuarter = new LinkedHashMap<>();
    private LinkedHashMap<String, Float> lastHalfAYear = new LinkedHashMap<>();
    private LinkedHashMap<String, Float> lastYear = new LinkedHashMap<>();


    private LinkedHashMap getLinkedHashMap(LinkedHashMap<String, Float> period) {
        values.forEach((k, v) -> {
            LocalDate date = LocalDate.parse(k.toString());
            if (date.compareTo(localDate) >= 0) {
                period.put(k.toString(), Float.valueOf(v.toString()));

            }
        });

        return period;
    }

    public LinkedHashMap getLastWeek() {

        this.localDate = LocalDate.now().minusWeeks(1).minusDays(1);
        return getLinkedHashMap(this.lastWeek);
    }

    public LinkedHashMap getLastTwoWeeks() {

        this.localDate = LocalDate.now().minusWeeks(2).minusDays(1);

        return getLinkedHashMap(this.lastTwoWeeks);
    }

    public LinkedHashMap getLastMonth() {

        this.localDate = LocalDate.now().minusMonths(1).minusDays(1);

        return getLinkedHashMap(this.lastMonth);
    }

    public LinkedHashMap getLastQuarter() {

        this.localDate = LocalDate.now().minusMonths(4).minusDays(1);
        return getLinkedHashMap(this.lastQuarter);
    }

    public LinkedHashMap getHalfOfYear() {

        this.localDate = LocalDate.now().minusMonths(6).minusDays(1);
        return getLinkedHashMap(this.lastHalfAYear);
    }

    public LinkedHashMap getLastYear() {

        this.localDate = LocalDate.now().minusYears(1).minusDays(1);
        return getLinkedHashMap(this.lastYear);
    }


    public LinkedHashMap fetchData(String rawXml) {
        this.values = new LinkedHashMap<>();

        if (rawXml == null) return null;
        mids = parseXMLMid(rawXml, "Mid");
        effectiveDates = parseXMLDate(rawXml, "EffectiveDate");
        for (int i = 0; i < mids.length; i++) {
            values.put(effectiveDates[i], mids[i]);
        }
        return values;
    }

    private static float[] parseXMLMid(String input, String tag) {

        String[] rates = input.split("<" + tag + ">");

        float[] numeralRates = new float[rates.length - 1];
        for (int i = 1; i < rates.length; i++) {

            numeralRates[i - 1] = Float.parseFloat(rates[i].substring(0, 6));
        }
        return numeralRates;
    }


    private static String[] parseXMLDate(String input, String tag) {

        String[] date = input.split("<" + tag + ">");
        String[] dates = new String[date.length - 1];
        for (int i = 1; i < date.length; i++) {

            dates[i - 1] = String.valueOf(date[i].substring(0, 10));
        }
        return dates;
    }
}
